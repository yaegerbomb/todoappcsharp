# README #

An ASP.Net 4.5 C# WebApi - Single Page implementation of the standard To-Do app.

The app stores the To-Do list in memory using a ConcurrentDictionary mocking a database implementation wrapped in a repository. 

The only required field of the model to pass through the API is that the title cannot be null.

The following implementation's frontend was built following the [John Papa's Angular 1.X style guide](https://github.com/johnpapa/angular-styleguide/tree/master/a1).

### Frameworks, Libraries, and Languages ###

* git
* C#
* ASP.Net 4.5
* WebApi
* javascript
* AngularJS
* css
* Bootstrap

### Setup ###

* git clone https://yaegerbomb@bitbucket.org/yaegerbomb/todoappcsharp.git
* Enable nuget package restore
* Build solution
* Run on local host
* Running demo should be on azure here: [http://todocsharp.azurewebsites.net/](http://todocsharp.azurewebsites.net/)


### Gifs ###
![6d19a2625a85b7bc25438af9894f1ff1.gif](https://bitbucket.org/repo/AbB5yx/images/3272040492-6d19a2625a85b7bc25438af9894f1ff1.gif)
![7d90b7f3952210d7d646b1c71370e25d.gif](https://bitbucket.org/repo/AbB5yx/images/3370240277-7d90b7f3952210d7d646b1c71370e25d.gif)
![869b9e8d3a9c8ed3aff0738a6651ab35.gif](https://bitbucket.org/repo/AbB5yx/images/2422719301-869b9e8d3a9c8ed3aff0738a6651ab35.gif)
![b34c059a92c24c134f008c5680069eda.gif](https://bitbucket.org/repo/AbB5yx/images/3005554627-b34c059a92c24c134f008c5680069eda.gif)