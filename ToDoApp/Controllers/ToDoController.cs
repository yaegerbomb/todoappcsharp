﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToDoApp.Models;
using ToDoApp.Repositories.ToDo;

namespace ToDoApp.Controllers
{
    public class ToDoController : ApiController
    {
        private ToDoRepository toDoRepository;

        public ToDoController()
        {
            this.toDoRepository = new ToDoRepository();
        }

        // GET api/todo
        public IEnumerable<ToDoItem> Get()
        {
            return toDoRepository.GetAll();
        }

        // GET api/todo/guid
        public ToDoItem Get(string id)
        {
            Guid toDoId = new Guid(id);
            return toDoRepository.Find(toDoId);
        }

        // POST api/todo
        public HttpResponseMessage Post(ToDoItem toDoItem)
        {
            if (ModelState.IsValid)
            {
                toDoRepository.Add(toDoItem);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // Put api/todo
        public HttpResponseMessage Put(ToDoItem toDoItem)
        {
            if (ModelState.IsValid)
            {
                toDoRepository.Update(toDoItem);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/values
        public ToDoItem Delete(string id)
        {
            Guid toDoId = new Guid(id);
            return toDoRepository.Remove(toDoId);
        }

    }
}
