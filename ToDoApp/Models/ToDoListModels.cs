﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoApp.Models
{
    public class ToDoListModels
    {
    }

    public class ToDoItem
    {
        public Guid Key { get; set; }
        [Required]
        public string TaskEntry { get; set; }
        public string Details { get; set; }
        public bool IsComplete { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public DateTime? CompletedAt { get; set; }
        public DateTime? DeadlineAt { get; set; }

        //consider adding a IsDeleted flag instead so we don't lose data
        public bool IsDeleted { get; set; }
    }
}