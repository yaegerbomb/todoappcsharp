﻿(function () {
    'use strict';

    angular.module('app.core', [
        'ui.bootstrap'   // ui-bootstrap (ex: carousel, pagination, dialog)
    ]);
})();