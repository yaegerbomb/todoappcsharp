﻿(function () {
    'use strict';

    angular
        .module("app.widgets", ['app.data'])
        .directive('todosWidget', todoWidget)

    function todoWidget() {
        var directive = {
            retrict: 'E',
            templateUrl: '/app/components/todo.directive.html',
            controller: ToDoController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        ToDoController.$inject = ['dataservice', '$uibModal'];

        function ToDoController(dataservice, $uibModal) {
            var vm = this;
            
            vm.AddToDo = AddToDo;
            vm.UpdateToDo = UpdateToDo;
            vm.UpdateToDoDialog = UpdateToDoDialog;
            vm.DeleteToDo = DeleteToDo;
            vm.IsPastDue = IsPastDue;
            vm.OpenToDoDetail = OpenToDoDetail;
            vm.CloseDetailDialog = CloseDetailDialog;
            vm.DetailsModel = {};
            vm.inlineOptions = {
                minDate: new Date(),
                showWeeks: true
            };
            vm.ModelErrorInline = false;
            vm.ModelErrorDialog = false;
            activate();

            function activate() {
                vm.NewTask = {};

                return dataservice.getToDos()
                    .then(function (data) {
                        vm.todos = data;
                        return vm.todos;
                    })
            }

            function AddToDo() {
                return dataservice.postToDo(vm.NewTask).then(function (data) {
                    if (data.status == 200) {
                        vm.ModelErrorInline = false;
                        activate();
                    } else {
                        vm.ModelErrorInline = true;
                    }
                })
            }

            function UpdateToDo(todo) {
                return dataservice.putToDo(todo).then(function (data) {
                    if (data.status == 200) {
                        vm.ModelErrorInline = false;
                        activate();
                    } else {
                        vm.ModelErrorInline = true;
                    }
                })
            }

            function UpdateToDoDialog() {
                return dataservice.putToDo(vm.activeToDo).then(function (data) {
                    if (data.status == 200) {
                        CloseDetailDialog();
                        vm.ModelErrorDialog = false;
                        activate();
                    } else {
                        vm.ModelErrorDialog = true;
                    }
                })
            }


            function DeleteToDo(id) {
                return dataservice.deleteToDo(id).then(function (data) {
                    activate();
                })
            }

            function IsPastDue(todo) {
                //might need to use UTC time for javascript?
                var now = new Date();
                var then = new Date(todo.DeadlineAt);
                return (then < now && !todo.IsComplete);
            }

            function OpenToDoDetail(todo) {
                vm.activeToDo = angular.copy(todo);
                vm.DetailsModel = $uibModal.open({
                    templateUrl: '/app/components/todos.detail.directive.html'
                });
            }

            function CloseDetailDialog() {
                vm.activeToDo = {};
                vm.DetailsModel.dismiss('cancel');
            }

        }
    }

})();