﻿(function () {
    'use strict';

    angular
        .module('app.data', [])
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$http'];

    function dataservice($http) {
        return {
            getToDos: getToDos,
            getToDo: getToDo,
            postToDo: postToDo,
            putToDo: putToDo,
            deleteToDo: deleteToDo
        };

        function getToDos() {
            return $http.get('/api/todo')
                .then(getToDosComplete)
                .catch(getToDosFailed);

            function getToDosComplete(response) {
                return response.data;
            }

            function getToDosFailed(error) {
                //handle error logic
                console.log(error);
                return error.data;
            }
        }

        function getToDo(id) {
            return $http.get('/api/todo/' + id)
                .then(getToDoComplete)
                .catch(getToDoFailed);

            function getToDoComplete(response) {
                return response.data;
            }

            function getToDoFailed(error) {
                //handle error logic
                console.log(error);
                return error.data;
            }
        }

        function postToDo(toDo) {
            return $http.post('/api/todo', toDo)
                .then(postToDoComplete)
                .catch(postToDoFailed);

            function postToDoComplete(response) {
                return response;
            }

            function postToDoFailed(error) {
                //handle error logic
                console.log(error);
                return error;
            }
        }

        function putToDo(toDo) {
            return $http.put('/api/todo', toDo)
                .then(putToDoComplete)
                .catch(putToDoFailed);

            function putToDoComplete(response) {
                return response;
            }

            function putToDoFailed(error) {
                //handle error logic
                console.log(error);
                return error;
            }
        }

        function deleteToDo(id) {
            return $http.delete('/api/todo/' + id)
                .then(deleteToDoComplete)
                .catch(deleteToDoFailed);

            function deleteToDoComplete(response) {
                return response.data;
            }

            function deleteToDoFailed(error) {
                //handle error logic
                console.log(error);
                return error.data;
            }
        }
    }

})();