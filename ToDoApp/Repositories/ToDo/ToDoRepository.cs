﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoApp.Models;

namespace ToDoApp.Repositories.ToDo
{
    public class ToDoRepository: IToDoRepository
    {
        static ConcurrentDictionary<Guid, ToDoItem> todos = new ConcurrentDictionary<Guid, ToDoItem>();

        public ToDoRepository()
        {
        }

        public IEnumerable<ToDoItem> GetAll()
        {
            return todos.Values;
        }

        public void Add(ToDoItem item)
        {
            item.Key = Guid.NewGuid();
            item.CreatedAt = DateTime.UtcNow;
            todos[item.Key] = item;
        }

        public ToDoItem Find(Guid key)
        {
            ToDoItem item;
            todos.TryGetValue(key, out item);
            return item;
        }

        public ToDoItem Remove(Guid key)
        {
            ToDoItem item;
            todos.TryGetValue(key, out item);
            todos.TryRemove(key, out item);
            return item;
        }

        public void Update(ToDoItem item)
        {
            item.ModifiedAt = DateTime.UtcNow;
            if (item.IsComplete)
            {
                item.CompletedAt = DateTime.UtcNow;
            }
            todos[item.Key] = item;
        }
    }
}