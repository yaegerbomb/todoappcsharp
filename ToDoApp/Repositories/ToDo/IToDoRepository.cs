﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoApp.Models;

namespace ToDoApp.Repositories
{
    public interface IToDoRepository
    {
        void Add(ToDoItem item);
        IEnumerable<ToDoItem> GetAll();
        ToDoItem Find(Guid key);
        ToDoItem Remove(Guid key);
        void Update(ToDoItem item);
    }
}