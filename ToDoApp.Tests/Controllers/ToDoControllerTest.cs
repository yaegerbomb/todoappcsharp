﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToDoApp.Controllers;
using System.Linq;
using ToDoApp.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace ToDoApp.Tests.Controllers
{
    [TestClass]
    public class ToDoControllerTest
    {
        [TestMethod]
        public void Get()
        {
            ToDoController controller = new ToDoController();
            controller.Request = new System.Net.Http.HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            IEnumerable<ToDoItem> toDoItems = controller.Get();

            Assert.IsNotNull(toDoItems);
            Assert.AreEqual(0, toDoItems.Count());

            Guid newTaskGuid = Guid.NewGuid();
            string taskEntry = "My First Task";
            ToDoItem newItem = new ToDoItem()
            {
                IsComplete = false,
                TaskEntry = taskEntry
            };
            var response = controller.Post(newItem);
            toDoItems = controller.Get();
            Assert.AreEqual("My First Task", toDoItems.ElementAt(0).TaskEntry);
        }

        [TestMethod]
        public void GetById()
        {
            ToDoController controller = new ToDoController();
            controller.Request = new System.Net.Http.HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // try to get an item with an empty guid - does not exist
            ToDoItem notReal = controller.Get(Guid.Empty.ToString());
            Assert.IsNull(notReal);

            //try to get an item that does exist
            ToDoItem isReal = controller.Get().FirstOrDefault();
            Guid key = isReal.Key;
            ToDoItem testIsReal = controller.Get(isReal.Key.ToString());
            Assert.IsNotNull(testIsReal);
        }

        [TestMethod]
        public void Post()
        {
            ToDoController controller = new ToDoController();
            controller.Request = new System.Net.Http.HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            //try to add a new to do item
            Guid newTaskGuid = Guid.NewGuid();
            string taskEntry = "This is my test entry";
            string details = "These are the details of my task";
            ToDoItem newItem = new ToDoItem()
            {
                IsComplete = false,
                TaskEntry = taskEntry,
                Details = details
            };
            controller.Post(newItem);

            newItem = controller.Get().Where(t => t.TaskEntry == "This is my test entry").FirstOrDefault();

            //make sure the item was actually added
            Assert.IsNotNull(newItem.TaskEntry);

            //check to make sure that the CreatedAt datetime field is before our current time
            Assert.IsTrue(controller.Get().Last().CreatedAt <= DateTime.UtcNow);

            Assert.IsTrue(newItem.Details == details);
        }

        [TestMethod]
        public void Put()
        {
            ToDoController controller = new ToDoController();
            controller.Request = new System.Net.Http.HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            ToDoItem toDoItem = controller.Get().FirstOrDefault();

            string taskEntry = "I have changed my task entry";
            string details = "I have change my details entry";
            toDoItem.TaskEntry = taskEntry;
            controller.Put(toDoItem);


            ToDoItem checkUpdate = controller.Get(toDoItem.Key.ToString());
            Assert.AreEqual(taskEntry, checkUpdate.TaskEntry);

            checkUpdate.Details = details;
            controller.Put(checkUpdate);
            checkUpdate = controller.Get(toDoItem.Key.ToString());
            Assert.AreEqual(details, checkUpdate.Details);

            //check to make sure that our modified time has been changed
            Assert.IsNotNull(checkUpdate.ModifiedAt);

            //update task to comepleted
            checkUpdate.IsComplete = true;
            controller.Put(checkUpdate);            
            checkUpdate = controller.Get(toDoItem.Key.ToString());
            Assert.IsNotNull(checkUpdate.CompletedAt);
        }


        [TestMethod]
        public void Delete()
        {
            ToDoController controller = new ToDoController();

            ToDoItem toDoItem = controller.Get().FirstOrDefault();

            //remove the first item in the list that is there by default
            ToDoItem toDoToDelete = controller.Delete(toDoItem.Key.ToString());
            Assert.AreEqual(toDoItem, toDoToDelete);

            //try to remove something that doesnt exist
            ToDoItem toDoDeleteNull = controller.Delete(Guid.Empty.ToString());
            Assert.AreEqual(null, toDoDeleteNull);
        }

    }
}
